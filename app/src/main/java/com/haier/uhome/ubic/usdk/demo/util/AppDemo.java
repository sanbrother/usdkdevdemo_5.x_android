package com.haier.uhome.ubic.usdk.demo.util;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.haier.uhome.trace.api.Trace;
import com.haier.uhome.ubic.usdk.demo.MakeDevice2NetSmartLink;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.http.UserAccount;
import com.haier.uhome.usdk.api.ConfigurableDevice;
import com.haier.uhome.usdk.api.interfaces.IDeviceScanListener;
import com.haier.uhome.usdk.api.interfaces.IuSDKCallback;
import com.haier.uhome.usdk.api.uSDKDeviceManager;
import com.haier.uhome.usdk.api.uSDKErrorConst;
import com.haier.uhome.usdk.api.uSDKManager;

public class AppDemo extends Application {

	public static final String TAG = "uSDK5.X Demo";
	private Handler mainActivityHandler = null;
	public void setMainActivityMsgHandler(Handler handler) {
		mainActivityHandler = handler;
	}

	private String[] permissionStringArray = null;
	public String[] getPermissionStringArray() {
		return permissionStringArray;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		uSDKManager sdkMgr = uSDKManager.getSingleInstance();
		sdkMgr.init(this);
		uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
		deviceManager.setKeepLocalOnline(true);
		setuSDKFeature(sdkMgr);
		/**
		 * 需要集成数据统计分析SDK
		 * 才能上传数据到链式跟踪
		 */
		//trace = Trace.createTrace(UTRACE_BIND_BUSSINESS_ID);

	}


	/**
	 * 启用链式跟踪系统，
	 * @param sdkManager
     */
	private void setuSDKFeature(uSDKManager sdkManager) {
		sdkManager.enableFeatures(uSDKManager.SDK_FEATURE_NONE);
	}

	private class HelpSmartDeviceWaiting2JoinWifiUtil implements IDeviceScanListener {

		private static final long serialVersionUID = 3899276926320512661L;

		@Override
		public void onDeviceRemoved(ConfigurableDevice candidater) {
			//getDevIdSuffix()用于smartlink指定mac配置
			System.out.println(candidater.getDevIdSuffix() +
					" has been disappeared!");
			//deviceid用于极路由功能开发
			System.out.println(candidater.getDevId());
			NotificationManager notificationManager = (NotificationManager) 
					getSystemService(Context.NOTIFICATION_SERVICE); 
			notificationManager.cancel(100);
		
		}

		/*
		@Override
		public void onPermissionDenied(String[] notGrantedPermission, String[] failedGrantedPermission) {
			permissionStringArray = failedGrantedPermission;
			if(mainActivityHandler != null) {
				mainActivityHandler.obtainMessage().sendToTarget();
			}

		}*/

		@Override
		public void onDeviceScanned(ConfigurableDevice candidater) {
			System.out.println("uSDK3.x Demo find:" + candidater.getDevIdSuffix());
			popNotiFoundunConfigedSmartDevice(candidater);
			
		}
	}
	
	//private HelpSmartDeviceWaiting2JoinWifiUtil helperUtil =
	//					new HelpSmartDeviceWaiting2JoinWifiUtil();

	/**
	 * <pre>
	 * Some U+ smart devices support wifi discovery mode, 
	 * we could find a wifi that it's name is uplus-xxxx-xxxx.
	 * It will join your home, if you smartlink it. 
	 * uSDK3.x support this function, steps are:
	 * 
	 * 一些未入網的智能設備上電后會對外廣播自己，如果此時用手機查看可以
	 * 看到一個WIFI，名稱是uplus-xxxx-xxxx，這時只要執行smartlink，
	 * 就可以把它配置入網。按如下步驟編程，實現發現待配置設備：
	 * 
	 * Step:
	 * 1.registerDeviceScanListener 注冊
	 * 2.startScanConfigurableDevice 開始掃描
	 * 3.stopScan 結束掃描，退出掃描自發現設備
	 * </pre>
	 */
	public void helpSmartDeviceAutoJoinWifi(uSDKDeviceManager uSDKDeviceMgr) {
		/*
		uSDKDeviceMgr.setDeviceScanListener(helperUtil);
		uSDKDeviceMgr.startScanConfigurableDevice(this, new IuSDKCallback() {
			@Override
			public void onCallback(uSDKErrorConst result) {
				//return error if wifi is off
				if(result == uSDKErrorConst.RET_USDK_OK) {

				}
			}
		});*/

	}
	
	/**
	 * <pre>
	 * stop uSDK3.x scanning smart devices waiting join wifi.
	 * 停止uSDK繼續掃描自發現智能設備。
	 * </pre>
	 */
	public void endScanWaitingSmartDevice(uSDKDeviceManager uSDKDeviceMgr) {
		uSDKDeviceMgr.stopScanConfigurableDevice(new IuSDKCallback() {
			@Override
			public void onCallback(uSDKErrorConst uSDKErrorConst) {

			}
		});
	}
	
	private void popNotiFoundunConfigedSmartDevice(ConfigurableDevice candidater) {
		String contentTitle = candidater.getDevIdSuffix() +
				getString(R.string.tip_smartdevice_noti_appear);
		String contentText = getString(R.string.tip_click_2_smartlink);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle(contentTitle)
						.setContentText(contentText);

		Intent resultIntent = new Intent(this, MakeDevice2NetSmartLink.class);
		resultIntent.putExtra("mac", candidater.getDevIdSuffix());
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(MakeDevice2NetSmartLink.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
				stackBuilder.getPendingIntent(
						0,
						PendingIntent.FLAG_ONE_SHOT
				);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
				(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(100, mBuilder.build());

	}

	//Demo没有集成uAnalytics3.1.01，实际使用uTrace时需要集成
	//Demo runs without uAnalytics. Get it when use uTrace
	private static final String UTRACE_BIND_BUSSINESS_ID = "bind";
	public static Trace trace;

	private UserAccount userAccount = new UserAccount();

	public UserAccount getUserAccount() {
		return userAccount;
	}


}

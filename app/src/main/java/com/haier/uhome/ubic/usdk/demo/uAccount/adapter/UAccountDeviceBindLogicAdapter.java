package com.haier.uhome.ubic.usdk.demo.uAccount.adapter;

import android.content.Context;

import com.haier.uhome.account.api.interfaces.IAccountListener;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.util.Util;

/**
 * Created by atxd163 on 16-9-17.
 */
public abstract class UAccountDeviceBindLogicAdapter implements IAccountListener<RespCommonModel> {
    @Override
    public void onResponseSuccess(RespCommonModel data) {
        actionLogic(data);
    }

    @Override
    public void onResponseFailed(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx,
                ctx.getString(R.string.tip_webquest_error)
                        + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    @Override
    public void onHttpError(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx, ctx.getString(R.string.tip_http_error)
                + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    public abstract void actionLogic(RespCommonModel data);

    public UAccountDeviceBindLogicAdapter(Context ctx) {
        this.ctx = ctx;
    }

    private Context ctx;
}

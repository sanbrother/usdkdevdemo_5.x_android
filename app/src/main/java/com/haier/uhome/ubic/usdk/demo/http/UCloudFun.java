package com.haier.uhome.ubic.usdk.demo.http;

import android.content.Context;

import com.haier.uhome.ubic.usdk.demo.util.AppDemo;
import com.haier.uhome.ubic.usdk.demo.util.Util;
import com.haier.uhome.usdk.api.interfaces.IuSDKCallback;
import com.haier.uhome.usdk.api.uSDKDevice;
import com.haier.uhome.usdk.api.uSDKDeviceManager;
import com.haier.uhome.usdk.api.uSDKErrorConst;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <pre>
 * U+ Cloud Action:
 * 1.Account Login U+ Cloud
 *  1.1 get devices of account
 *  1.2 add devices to account
 *  1.3 delete devices from account
 * 2.Account Logout Cloud
 * </pre>
 */
public class UCloudFun {
    /**
     * U+ Cloud Account Function URL
     */
    private static final String LOGIN_URL = "https://uhome.haier.net:6503/openapi/v2/user/login";
    private static final String LOGOUT_URL	= "https://uhome.haier.net:6503/openapi/v2/user/logout";
    private static final String GET_ACCOUNT_DEVICES = "https://uhome.haier.net:6503/openapi/v2/device/queryList";
    private static final String DELETE_ACCOUNT_DEVICE = "https://uhome.haier.net:6503/openapi/v2/device/unbind";
    private static final String ADD_ACCOUNT_DEVICE = "https://uhome.haier.net:6503/openapi/v2/device/bind";

    /**
     * U+ Cloud RemoteServer Domain And Port
     */
    private static final String REMOTE_SERVER_DOMAIN = "gw.haier.net";
    private static final int REMOTE_SERVER_PORT = 56811;

    public void accountLogin(final String username, String password, Context context, final IhttpActionResultCallback callback) {

        OpenApiV2HttpUtil dataBuilder = new OpenApiV2HttpUtil();
        AsyncHttpClient client = getHttpClient(context);

        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        final UserAccount userAccount = appDemo.getUserAccount();
        String loginDataStr = dataBuilder.
                buildLoginContent(username, password);

        Map<String, String> headerMap =
                dataBuilder.buildCommonHeader("", loginDataStr, context);
        Set<String> keys = headerMap.keySet();
        for(String str : keys) {
            client.addHeader(str, headerMap.get(str));
        }

        try{
            HttpEntity loginEntity = new StringEntity(loginDataStr, "UTF-8");
            client.post(context, LOGIN_URL,
                    loginEntity, "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers,
                                              JSONObject response) {
                            System.out.println("Login Action:" + response);

                            try {
                                String retCode = response.getString("retCode");
                                if(retCode != null && retCode.equals("00000")) {
                                    String userId = response.getString("openId");
                                    userAccount.setUserId(userId);
                                    userAccount.setUserName(username);

                                    String session = Util.getUserSession(headers);
                                    userAccount.setSession(session);

                                    if(callback != null) {
                                        callback.loginSuccess();
                                    }

                                } else {
                                    httpActionFailed(callback, response.toString());

                                }

                            } catch (JSONException e) {}

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers,
                                              String responseString, Throwable throwable) {
                            System.out.println("U+ Cloud Access Exception:" + throwable.getMessage());
                            httpActionFailed(callback, "Account Login Request Exception!");

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });

        } catch(Exception e) {
            e.printStackTrace();

        }

    }

    public void accountLogout(Context context, final IhttpActionResultCallback callback) {
        OpenApiV2HttpUtil dataBuilder = new OpenApiV2HttpUtil();
        AsyncHttpClient client = getHttpClient(context);
        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        final UserAccount userAccount = appDemo.getUserAccount();


        String logoutStr = "";

        String accessToken = userAccount.getSession();
        Map<String, String> headerMap
                = dataBuilder.buildCommonHeader(accessToken, logoutStr, context);
        Set<String> keys = headerMap.keySet();
        for(String str : keys) {
            client.addHeader(str, headerMap.get(str));
        }

        try{
            HttpEntity logoutEntity = new StringEntity(logoutStr, "UTF-8");
            client.post(context, LOGOUT_URL,
                    logoutEntity, "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode,
                                              Header[] headers, JSONObject response) {
                            System.out.println("Logout Action:" + response);

                            try {
                                String retCode = response.getString("retCode");
                                if(retCode != null && retCode.equals("00000")) {
                                    userAccount.setSession(null);
                                    userAccount.setUserName(null);
                                    userAccount.setUserId(null);
                                    userAccount.setDevicesJsonArray(null);

                                    if(callback != null) {
                                        callback.logoutSuccess();
                                    }

                                } else {
                                    httpActionFailed(callback, response.toString());

                                }

                            } catch (JSONException e) {}

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers,
                                              String responseString, Throwable throwable) {
                            System.err.println("U+ Cloud Access Exception:" + throwable.getMessage());
                            httpActionFailed(callback, "Account Logout Request Exception!");

                        }

                    });
        } catch(Exception e) {}

    }

    public void getDevicesOfAccountAndConnect2RemoteServer(
            final Context context, final IhttpActionResultCallback callback) {
        OpenApiV2HttpUtil dataBuilder = new OpenApiV2HttpUtil();
        AsyncHttpClient client = getHttpClient(context);
        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        final UserAccount userAccount = appDemo.getUserAccount();

        String getDevicesOfAccountJson = dataBuilder.buildFetchAllDevices();
        HttpEntity getDevicesOfAccountJsonEntity = null;
        try {
            getDevicesOfAccountJsonEntity
                    = new StringEntity(getDevicesOfAccountJson, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String acceptToken = userAccount.getSession();
        Map<String, String> headerMap
                = dataBuilder.buildCommonHeader(acceptToken, getDevicesOfAccountJson, context);
        Set<String> keys = headerMap.keySet();
        for(String str : keys) {
            client.addHeader(str, headerMap.get(str));
        }

        client.post(context, GET_ACCOUNT_DEVICES,
                getDevicesOfAccountJsonEntity, "application/json;charset=UTF-8", new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                httpActionFailed(callback, "Get SmartDevices Of Account Web Request Failed!");

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                if (200 == statusCode) {
                    try {
                        System.out.println("Get Accout Devices Action:" + response);

                        String retCode = response.getString("retCode");
                        if ("00000".equals(retCode) || "A00001-01003".equals(retCode)) {
                            JSONArray devicesJsonArray = response.getJSONArray("devices");
                            if (devicesJsonArray != null && devicesJsonArray.length() != 0) {
                                userAccount.setDevicesJsonArray(devicesJsonArray);

                            } else if(devicesJsonArray != null && devicesJsonArray.length() == 0) {
                                //devicesJsonArray == 0时，賬號沒有在U+云上添加設備數據
                                userAccount.setDevicesJsonArray(null);

                            }

                            //回调
                            if (callback != null) {
                                callback.getDevicesOfAccountSucess();
                            }

                            //连接remote server
                            /*==========================================================
                             *產品開發測試階段連接 U+開發者環境，發布時連接生產環境：
                             * deviceManager.connectToGateway(userAccount.getSession(),
                             *       "usermg.uopendev.haier.net", 56821,
                             *       remotedCtrledDeviceCollection, new IuSDKCallback() {
                             * 開發者環境（App測試階段）：usermg.uopendev.haier.net 56821
                             * 生產環境（App發布）：gw.haier.net 56811
                             *==========================================================
                             */
                            List remotedCtrledDeviceCollection = Util.fillDeviceInfoForRemoteControl(devicesJsonArray);
                            uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
                            deviceManager.connectToGateway(userAccount.getSession(),
                                    "gw.haier.net", 56811, remotedCtrledDeviceCollection, new IuSDKCallback() {
                                        @Override
                                        public void onCallback(uSDKErrorConst result) {
                                            if (result != uSDKErrorConst.RET_USDK_OK) {
                                                System.err.println("uSDK3XDemo conn U+ cloud server error:" + result);

                                            }
                                        }
                                    });

                        } else {
                            httpActionFailed(callback, response.toString());

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }

            }
        });
    }

    public boolean addDevice2Account(final Context context, uSDKDevice ready2AccountDevice, final IhttpActionResultCallback callback) {

        OpenApiV2HttpUtil dataBuilder = new OpenApiV2HttpUtil();
        AsyncHttpClient client = getHttpClient(context);

        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        final UserAccount userAccount = appDemo.getUserAccount();
        String acceptToken = userAccount.getSession();

        List<uSDKDevice> ready2AccountDeviceCollection = new ArrayList<uSDKDevice>();
        ready2AccountDeviceCollection.add(ready2AccountDevice);
        String addDevice2AccountJsonStr =
                dataBuilder.buildBindDeviceContent(acceptToken, ready2AccountDeviceCollection);

        Map<String, String> headerMap
                = dataBuilder.buildCommonHeader(acceptToken, addDevice2AccountJsonStr, context);
        Set<String> keys = headerMap.keySet();
        for(String str : keys) {
            client.addHeader(str, headerMap.get(str));
        }

        HttpEntity addDevice2AccountEntity = null;
        try{
            addDevice2AccountEntity = new StringEntity(addDevice2AccountJsonStr, "UTF-8");

        } catch(Exception e) {}

        client.post(context, ADD_ACCOUNT_DEVICE,
                addDevice2AccountEntity, "application/json;charset=UTF-8", new JsonHttpResponseHandler(){

                    @Override
                    public void onSuccess(int statusCode, Header[] headers,
                                          JSONObject response) {
                        System.out.println("StatusCode:" + statusCode);

                        try {
                            System.out.println("Add SmartDevice to Account Action:" + response);

                            String retCode = response.getString("retCode");
                            if(retCode != null && retCode.equals("00000")) {

                                //重新连接设备列表
                                if(callback != null) {
                                    callback.addDevice2AccountSucess();
                                }

                                getDevicesOfAccountAndConnect2RemoteServer(context, null);

                            } else {
                                httpActionFailed(callback, response.toString());

                            }


                        } catch (JSONException e) {}

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          String responseString, Throwable throwable) {
                        System.out.println("U+ Cloud Access Exception:" + throwable.getMessage());
                        httpActionFailed(callback, "Add SmartDevice to Account Failed!");

                    }
                });

        return false;
    }

    public void removeDeviceFromAccount(
            final Context context, uSDKDevice smartDevice, final IhttpActionResultCallback callback) {
        String deviceId = smartDevice.getDeviceId();

        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        final UserAccount userAccount = appDemo.getUserAccount();

        String acceptToken = userAccount.getSession();

        OpenApiV2HttpUtil dataBuilder = new OpenApiV2HttpUtil();
        AsyncHttpClient client = getHttpClient(context);

        String deleteAccountDeviceInfoJsonValue
                = dataBuilder.buildDeleteDeviceFromAccountContent(deviceId);

        Map<String, String> headerMap
                = dataBuilder.buildCommonHeader(acceptToken, deleteAccountDeviceInfoJsonValue, context);
        Set<String> keys = headerMap.keySet();
        for(String str : keys) {
            client.addHeader(str, headerMap.get(str));
        }

        HttpEntity deleteAccountDeviceInfoEntity = null;
        try {
           deleteAccountDeviceInfoEntity = new StringEntity(deleteAccountDeviceInfoJsonValue);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        client.post(context, DELETE_ACCOUNT_DEVICE,
                deleteAccountDeviceInfoEntity, "application/json;charset=UTF-8", new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                httpActionFailed(callback, "Remove Account's SmartDevice Failed!");

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                if (200 == statusCode) {
                    try {
                        System.out.println("Remove SmartDevice Action:" + response);

                        String retCode = response.getString("retCode");
                        if ("00000".equals(retCode)) {
                            if (callback != null) {
                                callback.removeDeviceFromAccountSucess();
                            }
                            getDevicesOfAccountAndConnect2RemoteServer(context, null);

                        } else {
                            httpActionFailed(callback, response.toString());

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }

                }

            }
        });

    }

    private void httpActionFailed(IhttpActionResultCallback callback, String msg) {
        if(callback != null) {
            callback.failed(msg);

        }

    }

    private AsyncHttpClient getHttpClient(Context context) {
        client = Util.setCertificates(context, client);
        return client;
    }

    private static AsyncHttpClient client = new AsyncHttpClient();
}

package com.haier.uhome.ubic.usdk.demo;

import android.app.Activity;
import android.os.Bundle;

import com.haier.uhome.trace.api.Trace;
import com.haier.uhome.trace.api.TraceNode;
import com.haier.uhome.trace.api.TraceProtocolConst;
import com.haier.uhome.ubic.usdk.demo.util.AppDemo;

/**
 * 链式跟踪示例伪代码
 */
public class UTraceExampleActivity extends Activity {

    private TraceNode getQRCodeBeginNode;
    private TraceNode getQRCodeEndNode;
    private TraceNode manSelectedBeginNode;
    private TraceNode manSelectedEndNode;
    private TraceNode reportDeviceInfoQuestBeginNode;
    private TraceNode reportDeviceInfoQuestEndNode;
    private Trace mTrace;
    private TraceNode uwsBegin;
    private TraceNode uwsFinalEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //绑定业务开始
        bindLogicBegin();
    }

    /**
     * 链式跟踪
     * 绑定业务开始
     */
    private void bindLogicBegin () {
        mTrace = Trace.createTrace("bind");
    }

    private void execManSelected() {
        manSelectedChoiceBegin();
        //…………手工选择过程
        manSelectedChoiceEnd();
    }

    private void execQRSelected() {
        dealQRCodeBegin();
        //…………扫码过程
        dealQRCodeEnd();
    }

    private void execReportDeviceInfo() {
        reportDeviceInfoBegin();
        //……上传设备信息
        reportDeviceInfoEnd();
    }

    private void execBindWithoutUSDK() {
        reportBindResultBegin();
        //……向uws绑定
        reportBindResultEnd();
    }

    /**
     * 手工选择开始
     * 环的名字要统一
     * usdk能知道的不用传
     */
    private void manSelectedChoiceBegin(){
        manSelectedBeginNode = TraceNode.createCSNode("manSelected", "http");
        manSelectedBeginNode.add(TraceProtocolConst.PRO_APP_ID, "MB-XXXXX-0000");
        manSelectedBeginNode.add(TraceProtocolConst.PRO_USER_ID, "U123456");
        mTrace.addTraceNode(manSelectedBeginNode);
    }

    /**
     * 手工选择结束
     * 有些字段APP商量，不够
     */
    private void manSelectedChoiceEnd() {
        manSelectedEndNode = TraceNode.createCRNode("manSelected", "http", manSelectedBeginNode);
        //扫码：1、手动选择：2？
        manSelectedEndNode.add("dmth", "2");
        //根据实际情况填写设备安全类型
        manSelectedEndNode.add(
                TraceProtocolConst.PRO_CONFIG_SECURITY_TYPE,
                TraceProtocolConst.PRO_CONFIG_SECURITY_TYPE_SECURE);
        mTrace.addTraceNode(manSelectedEndNode);

    }

    //------------------------------------------------------------------------//
    /**
     * 扫码开始
     */
    private void dealQRCodeBegin() {
        getQRCodeBeginNode =
                TraceNode.createCSNode("http://oid.haier.com/oid", "http");
        //APPID
        getQRCodeBeginNode.add(TraceProtocolConst.PRO_APP_ID, "MB-XXXXX-0000");
        //UserId
        getQRCodeBeginNode.add(TraceProtocolConst.PRO_USER_ID, "U123456");
        int result = mTrace.addTraceNode(getQRCodeBeginNode);
        if (result != 0) {
            //
        }

    }

    //------------------------------------------------------------------------//
    /**
     * 扫码请求结束
     */
    private void dealQRCodeEnd() {
        getQRCodeEndNode =
                TraceNode.createCRNode("http://oid.haier.com/oid", "http", getQRCodeBeginNode);
        //扫码内容
        getQRCodeEndNode.add("qrno", "D006iM$MFTKA$AM$M$cM$KM$KKM$=$AM$Kv=Y$AM$$K$KA$KKM$FATMAK$FK=$KMKKYKKL4");
        //扫码：1、手动选择：2？
        getQRCodeEndNode.add("dmth", "1");
        getQRCodeBeginNode.add(TraceProtocolConst.PRO_CODE, "00000");
        mTrace.addTraceNode(getQRCodeEndNode);

    }

    /**
     * -----------------------------汇报设备信息查询结果-----------------------
     */
    private void reportDeviceInfoBegin() {
        reportDeviceInfoQuestBeginNode =
                TraceNode.createCSNode("CheckModel", "http");
        mTrace.addTraceNode(reportDeviceInfoQuestBeginNode);
    }


    private void reportDeviceInfoEnd() {
        reportDeviceInfoQuestEndNode =
                TraceNode.createCRNode("CheckModel", "http", reportDeviceInfoQuestBeginNode);
        //-----------------------------------------------------------------
        //设备ID
        reportDeviceInfoQuestEndNode.add(
                TraceProtocolConst.PRO_DEVICE_ID, "mac");
        //型号：
        reportDeviceInfoQuestEndNode.add(TraceProtocolConst.MODEL, "");
        //是否网器？
        reportDeviceInfoQuestEndNode.add("isnb", "");
        //绑定方案：
        reportDeviceInfoQuestEndNode.add("proc", "3");
        //配置绑定方案版本号
        reportDeviceInfoQuestEndNode.add("pver", "2.0");
        //网器机编
        reportDeviceInfoQuestEndNode.add("bcode", "");

        //以上字段根据App问题分析需要自己添加
        //-------------------------------------------------------------
        //
        reportDeviceInfoQuestEndNode.add(
                TraceProtocolConst.PRO_CODE, "00000");
        //
        mTrace.addTraceNode(reportDeviceInfoQuestEndNode);

    }

    /**
     * 调用UWS前操作
     * 使用uSDKDevcieManager.bind方法，不用调用此步骤
     */
    private void reportBindResultBegin() {
        uwsBegin = TraceNode.createCSNode("http://uws.haier.net", "http");
        mTrace.addTraceNode(uwsBegin);

    }

    /**
     * 经过重试之后的绑定结果：
     * 使用uSDKDevcieManager.bind方法，不用调用此步骤
     */
    private void reportBindResultEnd() {
        uwsFinalEnd = TraceNode.createCRNode("http://uws.haier.net", "http", uwsBegin);
        uwsFinalEnd.add(TraceProtocolConst.PRO_CODE, "00000");
        /**出现错误时填写错误原因
        uwsFinalEnd.add(TraceProtocolConst.PRO_CODE,"12345");
        uwsFinalEnd.add(TraceProtocolConst.PRO_DESCRIPTION, "错误原因");*/
        mTrace.addTraceNode(uwsFinalEnd);

    }


}

package com.haier.uhome.ubic.usdk.demo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.haier.uhome.account.api.interfaces.IAccountListener;
import com.haier.uhome.account.api.uAccount;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.account.model.uacmodel.UacDevice;
import com.haier.uhome.ubic.usdk.demo.http.IhttpActionResultCallback;
import com.haier.uhome.ubic.usdk.demo.http.UCloudFun;
import com.haier.uhome.ubic.usdk.demo.http.UserAccount;
import com.haier.uhome.ubic.usdk.demo.util.AppDemo;
import com.haier.uhome.ubic.usdk.demo.util.Util;
import com.haier.uhome.usdk.api.interfaces.IuSDKCallback;
import com.haier.uhome.usdk.api.uSDKDeviceManager;
import com.haier.uhome.usdk.api.uSDKErrorConst;

import java.util.List;

/**
 * Account Login Logout Dialog
 * APP_ID、APP_KEY放置
 * 开发者环境如何设置
 * 获得我想知道的一切信息的类或接口
 */
public class AccountLoginoutDialog extends Dialog implements IhttpActionResultCallback {
    private EditText accountLoginUsernameEditText;
    private EditText accountLoginPasswordEditText;
    private Button accountLoginButton;
    private Button accountLogoutButton;
    private Context context;
    private uAccount accountHelper = uAccount.getSingleInstance();

    public AccountLoginoutDialog(Context context) {
        super(context);
        this.setContentView(R.layout.accoun_login);
        this.initUI();
        this.initUIEvent();
        this.context = context;

    }

    public void initUI() {
        accountLoginUsernameEditText = (EditText)
                findViewById(R.id.account_username_ev);

        accountLoginPasswordEditText = (EditText)
                findViewById(R.id.account_password_ev);

        accountLoginButton = (Button)
                findViewById(R.id.account_login_bt);

        accountLogoutButton = (Button)
                findViewById(R.id.account_logout_bt);

        dealLogin();
    }

    public void initUIEvent() {
        accountLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = accountLoginUsernameEditText.getText().toString();
                String password = accountLoginPasswordEditText.getText().toString();

                //参数名字顺序可以调整一下
                //deviceToken是啥，能不能去掉
                //登录成功后data里能不能同时携带userId
                //打印请求url和各项参数
                accountHelper.login(context, username, password, new IAccountListener<String>() {
                    @Override
                    public void onResponseSuccess(String data) {
                        System.out.println("" + data);
                        String accountToken = accountHelper.getAccessToken();
                        fillAccountToken(accountToken);

                    }

                    @Override
                    public void onResponseFailed(RespCommonModel respCommonModel) {
                        System.err.println("err result:" + respCommonModel);
                    }

                    @Override
                    public void onHttpError(RespCommonModel respCommonModel) {
                        System.err.println();
                    }

                });

                //UCloudFun uCloudFun = new UCloudFun();
                //uCloudFun.accountLogin(username, password, getContext(), AccountLoginoutDialog.this);
            }
        });

        accountLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //UCloudFun uCloudFun = new UCloudFun();
                //uCloudFun.accountLogout(getContext(), AccountLoginoutDialog.this);
                accountHelper.logout(context, new IAccountListener<RespCommonModel>() {
                    @Override
                    public void onResponseSuccess(RespCommonModel data) {
                        clearAccountTokenAndSetAccoutLogout();
                    }

                    @Override
                    public void onResponseFailed(RespCommonModel respCommonModel) {

                    }

                    @Override
                    public void onHttpError(RespCommonModel respCommonModel) {

                    }


                });
            }
        });

    }

    private void dealLogin() {
        AppDemo appDemo = (AppDemo) getContext().getApplicationContext();
        UserAccount account = appDemo.getUserAccount();
        if (account.isLogin() == true) {
            this.setTitle(account.getUserId()
                    + getContext().getString(R.string.code_tip_account_has_logined));

        }

    }

    @Override
    public void loginSuccess() {
        UCloudFun uCloudFun = new UCloudFun();
        uCloudFun.getDevicesOfAccountAndConnect2RemoteServer(
                getContext(), AccountLoginoutDialog.this);
        setTitle(getContext().getString(R.string.tip_login_ok));
        ((Activity) context).invalidateOptionsMenu();

    }

    @Override
    public void getDevicesOfAccountSucess() {
    }

    @Override
    public void logoutSuccess() {
        this.setTitle(this.getContext().getString(R.string.tip_logout_ok));
        ((Activity) context).invalidateOptionsMenu();
        uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
        deviceManager.disconnectToGateway(new IuSDKCallback() {
            @Override
            public void onCallback(uSDKErrorConst result) {
                System.out.println("exec disconnect to Remote Server:" + result);
            }
        });

    }

    @Override
    public void failed(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void removeDeviceFromAccountSucess() {

    }

    @Override
    public void addDevice2AccountSucess() {

    }

    /**
     * 登录成功后填充session
     */
    private void fillAccountToken(String session) {
        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        UserAccount account = appDemo.getUserAccount();
        account.setSession(session);

        //加载登录基本信息页面
        setTitle(getContext().getString(R.string.tip_login_ok));
        ((Activity) context).invalidateOptionsMenu();

        //
        getDevicesOfAccountAndConnect2RemoteServer();

    }

    /**
     * 注销成功清空账号token
     */
    private void clearAccountTokenAndSetAccoutLogout() {
        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        UserAccount account = appDemo.getUserAccount();
        account.setSession(null);
        account.setDevicesOfAccount(null);

        this.setTitle(this.getContext().getString(R.string.tip_logout_ok));
        ((Activity) context).invalidateOptionsMenu();

        //断开服务器连接
        turnOffRemoteServerConnection();

    }

    /**
     * 断开远程服务器连接
     */
    private void turnOffRemoteServerConnection() {
        uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
        deviceManager.disconnectToGateway(new IuSDKCallback() {
            @Override
            public void onCallback(uSDKErrorConst result) {
                if(result != uSDKErrorConst.RET_USDK_OK) {
                    System.err.println(
                            "uSDKDemo App exec disconnectToGateway failed: " + result.getErrorId());
                }
            }
        });
    };

    //maintype第二个参数填空的说明
    //是否再重载一个不带maintype和第二个参数的方法
    private void getDevicesOfAccountAndConnect2RemoteServer() {
        accountHelper.getDeviceList(context, null, null, new IAccountListener<UacDevice[]>() {
            @Override
            public void onResponseSuccess(UacDevice[]data) {
                System.out.println("message here");
                //如果数组个数为0时，是否直接返回null
                if(data != null) {
                    UacDevice[] devices = (UacDevice[]) data;
                    if(devices.length != 0) {
                        //更新设备信息并连接远程，另外只有两个设备
                        updateDevicesOfAccountAndConnect2RemoteServer(devices);

                    }
                }

            }

            @Override
            public void onResponseFailed(RespCommonModel data) {
                System.out.println(data);

            }

            @Override
            public void onHttpError(RespCommonModel respCommonModel) {

            }

        });
    }

    /**
     * 更新账号所属U+智能设备
     */
    private void updateDevicesOfAccountAndConnect2RemoteServer(UacDevice[] devicesOfAccount) {
        AppDemo appDemo = (AppDemo) context.getApplicationContext();
        UserAccount userAccount = appDemo.getUserAccount();
        userAccount.setDevicesOfAccount(devicesOfAccount);

        String token = userAccount.getSession();
        conect2remoteserver(token, devicesOfAccount);

    }

    private void conect2remoteserver(String token, UacDevice[] devicesOfAccount) {
        uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
        List remotedCtrledDeviceCollection = Util.fillDeviceInfoForRemoteControl(devicesOfAccount);
        deviceManager.connectToGateway(token, "gw.haier.net", 56811,
                remotedCtrledDeviceCollection,
                new IuSDKCallback() {

                @Override
                public void onCallback(uSDKErrorConst result) {
                     if(result != uSDKErrorConst.RET_USDK_OK) {
                        System.err.println("uSDK3XDemo exec connectToGateway method failed! " + result);
                     }
                }
        });

    }

}

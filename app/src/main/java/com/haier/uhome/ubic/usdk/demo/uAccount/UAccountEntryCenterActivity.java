package com.haier.uhome.ubic.usdk.demo.uAccount;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.haier.uhome.account.api.interfaces.IAccountListener;
import com.haier.uhome.account.api.uAccount;
import com.haier.uhome.account.model.uacmodel.UacDevice;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.ubic.usdk.demo.util.LoginDialog;
import com.haier.uhome.ubic.usdk.demo.MainActivity;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.http.UserAccount;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountGetUCloudDeviceListAdapter;
import com.haier.uhome.ubic.usdk.demo.util.ActionStepTipDialog;
import com.haier.uhome.ubic.usdk.demo.util.AppDemo;

/**
 * 演示uAccount Lib:
 * 登錄、找回密碼、注冊
 *
 * Express uAccount Lib function:
 * login, find password, register
 */
public class UAccountEntryCenterActivity extends Activity {
    private Button accountLoginButton;
    private EditText accountLoginUsernameEditText;
    private EditText accountLoginPasswordEditText;
    private Button accountRegisterButton;
    private Button accountForgetPasswordButton;

    private uAccount accountHelper = uAccount.getSingleInstance();

    private ActionStepTipDialog actionStepTipDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uaccount_entry_activity);
        init();
        initEvent();
    }

    private void init() {
        accountLoginUsernameEditText = (EditText)
                findViewById(R.id.account_username_ev);
        accountLoginPasswordEditText = (EditText)
                findViewById(R.id.account_password_ev);
        accountLoginButton = (Button)
                findViewById(R.id.account_login_bt);
        accountForgetPasswordButton = (Button)
                findViewById(R.id.account_forgetPassword_bt);
        accountRegisterButton = (Button)
                findViewById(R.id.account_register_bt);
    }

    private void initEvent() {
        accountLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = accountLoginUsernameEditText.getText().toString();
                String password = accountLoginPasswordEditText.getText().toString();


                /*
                actionStepTipDialog =
                        new ActionStepTipDialog(UAccountEntryCenterActivity.this);
                actionStepTipDialog.setActionStepTipInfo("Sign in...");
                */

                final LoginDialog loginDialog = new LoginDialog(UAccountEntryCenterActivity.this);
                loginDialog.show();
                loginDialog.setDialogContent("正在登录");

                /**
                 * 發生簽名錯誤時請檢查手機時間是否與服務器時間差5分鐘
                 * "簽名錯誤"：possible reaseon: abs(phone time - server time) > 5mins
                 */
                accountHelper.login(UAccountEntryCenterActivity.this, username, password, new IAccountListener<String>() {
                    @Override
                    public void onResponseSuccess(String data) {
                        System.out.println("" + data);
                        loginDialog.dismiss();
                        String accountToken = accountHelper.getAccessToken();
                        fillAccountToken(accountToken);

                    }

                    @Override
                    public void onResponseFailed(RespCommonModel respCommonModel) {
                        System.err.println("err result:" + respCommonModel);
                        //actionStepTipDialog.setActionStepTipInfo(respCommonModel.getRetInfo());
                        loginDialog.setDialogContent(respCommonModel.getRetInfo());
                    }

                    @Override
                    public void onHttpError(RespCommonModel respCommonModel) {
                        //actionStepTipDialog.setActionStepTipInfo(respCommonModel.getRetInfo());
                        loginDialog.setDialogContent(respCommonModel.getRetInfo());
                    }

                });

                //actionStepTipDialog.show();

            }
        });

        accountForgetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpUSmartOfficialSiteFindPassWord();

            }
        });

        accountRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent();
                registerIntent.setClass(getBaseContext(),
                        UAccountRegisterThenLoginActivity.class);
                startActivity(registerIntent);

            }
        });
    }

    /**
     * 跳轉官網找回密碼
     * jump to official site, find password
     */
    private void jumpUSmartOfficialSiteFindPassWord() {
        String forgetPasswordURL =
                accountHelper.findPassword(UAccountEntryCenterActivity.this);
        Intent forgetPasswordURLIntent = new Intent();
        forgetPasswordURLIntent.setAction(Intent.ACTION_VIEW);
        Uri forgetPasswordURI = Uri.parse(forgetPasswordURL);
        forgetPasswordURLIntent.setData(forgetPasswordURI);
        startActivity(forgetPasswordURLIntent);

    }

    /**
     * 獲得session值
     * get account session(acceptToken)
     */
    private void fillAccountToken(String session) {
        AppDemo appDemo = (AppDemo) getApplicationContext();
        UserAccount account = appDemo.getUserAccount();
        account.setSession(session);

        getDevicesOfAccount();

    }

    /**
     * 調用uAccount獲得賬號設備列表
     * Query Account's smart devices info
     */
    private void getDevicesOfAccount() {

        accountHelper.getDeviceList(UAccountEntryCenterActivity.this, null, null,
                new UAccountGetUCloudDeviceListAdapter(UAccountEntryCenterActivity.this) {
                    @Override
                    public void actionLogic(UacDevice[] devices) {
                        if (devices != null) {
                            if (devices.length != 0) {
                                updateDevicesOfAccount(devices);

                            } else {
                                updateDevicesOfAccount(null);

                            }

                        }

                        loginOKJumpToMainActivity();

                    }

                    /**
                     * 账号下无设备获取设备会返回A00001-01003
                     */
                    @Override
                    public void onResponseFailed(RespCommonModel respCommonModel) {
                        super.onResponseFailed(respCommonModel);
                        if (respCommonModel != null &&
                                "A00001-01003".equals(
                                        respCommonModel.getRetCode())) {
                            loginOKJumpToMainActivity();

                        }

                    }

                });

    }

    /**
     * 登錄成功跳轉至主視圖
     * jump to main activity with "login" info
     */
    private void loginOKJumpToMainActivity() {
        Intent loginedIntent = new Intent();
        loginedIntent.setClass(this, MainActivity.class);
        startActivity(loginedIntent);
    }

    /**
     * 更新賬號擁有的智能設備信息
     * refresh account's smart devices info
     */
    private void updateDevicesOfAccount(UacDevice[] devicesOfAccount) {
        AppDemo appDemo = (AppDemo) getApplicationContext();
        UserAccount userAccount = appDemo.getUserAccount();
        userAccount.setDevicesOfAccount(devicesOfAccount);

    }

}

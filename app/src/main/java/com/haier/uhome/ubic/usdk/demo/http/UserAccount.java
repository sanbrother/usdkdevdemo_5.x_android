package com.haier.uhome.ubic.usdk.demo.http;

import com.haier.uhome.account.model.uacmodel.UacDevice;
import com.haier.uhome.usdk.api.uSDKDevice;

import org.json.JSONArray;

/**
 * Express U+ Cloud Account
 *
 */
public class UserAccount {
    private String userName;
    private String password;
    private String userId;
    private String session;
    private JSONArray devicesJsonArray;

    private UacDevice[] devicesOfAccount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public JSONArray getDevicesJsonArray() {
        return devicesJsonArray;
    }

    public void setDevicesJsonArray(JSONArray devicesJsonArray) {
        this.devicesJsonArray = devicesJsonArray;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSmartDeviceBelongAccount(uSDKDevice smartDeviceItem) {
        String smartDeviceId = smartDeviceItem.getDeviceId();

        UacDevice[] deviceItems = getDevicesOfAccount();
        if(deviceItems != null) {
            for (int i = 0; i < deviceItems.length; i++) {
                UacDevice deviceItem = null;
                deviceItem = deviceItems[i];
                String deviceId = deviceItem.getId();
                if(smartDeviceId.equals(deviceId)) {
                    return true;

                }

            }

        }

        return false;

    }

    public boolean isLogin() {
        if(getSession() == null) {
            return false;

        } else {
            return true;

        }
    }

    public UacDevice[] getDevicesOfAccount() {
        return devicesOfAccount;
    }

    public void setDevicesOfAccount(UacDevice[] devicesOfAccount) {
        this.devicesOfAccount = devicesOfAccount;
    }
}

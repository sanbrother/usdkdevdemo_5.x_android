package com.haier.uhome.ubic.usdk.demo.http;

import android.content.Context;

import com.haier.uhome.usdk.api.uSDKDevice;
import com.haier.uhome.usdk.api.uSDKManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * Get APP_ID and APP_KEY from U+ Developement Site
 * </pre>
 */
public class OpenApiV2HttpUtil {

	//public static final String BASE_URL = "http://103.8.220.166:/";
	//public static final String APP_ID = "MB-DEMOANDROID-0000";
	//public static final String APP_KEY = "c23825ad3a4ec3ad3edda556cae7ba40";
	public static final String APP_ID = "MB-CAST-0000";
	public static final String APP_KEY = "7552701b32c3775bf885abbf19886025";
	//public static final String APP_ID = "MB-HLQ-0000";
	//public static final String APP_KEY = "26558cd75508126cd6118028bc80f831";
	//Test AppKey
	//public static final String APP_KEY = "e3e48e32fb6636328b9cfdf1decf753d";
	private static int sequenceIndex = 1;

	/**
	 * 生成登錄數據體
	 * make login body content
	 */
	public String buildLoginContent(String name, String pwd) {
		JSONObject job = new JSONObject();
		try {
			job.put("loginId", name);
			job.put("deviceToken", "");
			job.put("password", pwd);

		} catch (JSONException e) {
			e.printStackTrace();
			
		}
		return job.toString();
	}

	public static String genSequenceId() {
		String timestamp = genTimestamp();
		String sequenceId = timestamp + String.format("%06d", sequenceIndex++);
		return sequenceId;
	}

	public static String genTimestamp() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String timestamp = dateFormat.format(new Date());
		return timestamp;
	}

	public static String getTimestamplong() {
		return System.currentTimeMillis() + "";
	}

	/**
	 * 生成Sign字段值
	 * make sign value
	 */
	public static String genSign(String bodyJSON, String timeStamp) {
		StringBuffer signStringBuffer = new StringBuffer();

		signStringBuffer = signStringBuffer.append(bodyJSON)
				.append(APP_ID)
				.append(APP_KEY)
				.append(timeStamp);

		String signOrgData = signStringBuffer.toString();

		byte[] signByteArray = null;
		byte[] md5ByteArray = null;

		try {
			signByteArray = signOrgData.getBytes();
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			md5ByteArray = messageDigest.digest(signByteArray);

		} catch (Exception ex) {
			ex.printStackTrace();

		}

		StringBuffer signHexBuffer = new StringBuffer();
		for (int i = 0; i < md5ByteArray.length; i++) {
			String hexStr = Integer.toHexString(0xff & md5ByteArray[i]);
			if(hexStr.length() == 1) {
				signHexBuffer.append('0');
			}
			signHexBuffer.append(hexStr);
		}

		return signHexBuffer.toString();

	}

	/**
	 * 生成公共http頭
	 * prepare common header
	 */
	public Map<String, String> buildCommonHeader(String accessToken, String bodyJson, Context context) {
		Map<String, String> headerMap = new HashMap();
		headerMap.put("appId", APP_ID);
		headerMap.put("appVersion", "1.0");
		headerMap.put("clientId", getClientId(context));
		headerMap.put("sequenceId", genSequenceId());
		headerMap.put("accessToken", accessToken);

		String timeStamp = getTimestamplong();
		headerMap.put("sign", genSign(bodyJson, timeStamp));
		headerMap.put("timestamp", timeStamp);
		headerMap.put("language", "zh-cn");
		headerMap.put("timezone", "8");
		return headerMap;
	}

	/**
	 * uSDK provide clientId generator
	 * uSDK提供內置clientId生成方法
	 */
	private String getClientId(Context context) {
		uSDKManager uSDKMgr = uSDKManager.getSingleInstance();
		String clientId = uSDKMgr.getClientId(context);
		return clientId;
	}

	/**
	 * 拉取U+云用戶賬號設備數據
	 * get Account's Devices Info from U+ Cloud
	 */
	public String buildFetchAllDevices() {
		JSONObject job = new JSONObject();
		try {
			job.put("mainType", "");
			job.put("typeId", "");

		} catch (JSONException e) {
			e.printStackTrace();

		}

		return job.toString();
	}

	/**
	 * 刪除U+云用戶賬號設備
	 * delete Account's Device Info from U+ Cloud
	 */
	public String buildDeleteDeviceFromAccountContent(String deviceId) {
		JSONObject job = new JSONObject();
		try {
			job.put("deviceId", deviceId);

		} catch (JSONException e) {
			e.printStackTrace();

		}

		return job.toString();

	}

	/**
	 * 向U+云賬號添加設備信息
	 * Add SmartDevice Info to U+ Account
	 */
	public String buildBindDeviceContent(String session, List<uSDKDevice> devices) {
		JSONObject job = new JSONObject();
		JSONArray jArray = new JSONArray();
		try {
			for (uSDKDevice smartDevice : devices) {
				JSONObject deviceJob = new JSONObject();

				//添加id
				deviceJob.put("id", smartDevice.getDeviceId());

				//添加name
				deviceJob.put("name", "smartdevice");

				//添加typeInfo
				JSONObject typeInfoJson = new JSONObject();
				typeInfoJson.put("typeId", smartDevice.getUplusId());
				deviceJob.put("typeInfo", typeInfoJson);

				//添加deviceModules
				JSONArray deviceModuleArray = new JSONArray();
				deviceJob.put("deviceModules", deviceModuleArray);

				//添加Wifi的模組信息
				JSONObject wifimoduleJson = new JSONObject();
				deviceModuleArray.put(wifimoduleJson);

				//wifimodule有三個小項目：
				wifimoduleJson.put("moduleId", smartDevice.getDeviceId());
				wifimoduleJson.put("moduleType", "wifimodule");
				JSONArray wifimoduleInfoArray = new JSONArray();
				wifimoduleJson.put("moduleInfos", wifimoduleInfoArray);

				//拼裝moduleInfos信息wifimoduleJson
				//ip
				JSONObject info_ip = new JSONObject();
				info_ip.put("key", "ip");
				info_ip.put("value", smartDevice.getIp());
				wifimoduleInfoArray.put(info_ip);

				//hardwareVers
				JSONObject info_hardwareVers = new JSONObject();
				info_hardwareVers.put("key", "hardwareVers");
				info_hardwareVers.put("value", smartDevice.getSmartLinkHardwareVersion());
				wifimoduleInfoArray.put(info_hardwareVers);

				//softwareVers
				JSONObject info_softwareVers = new JSONObject();
				info_softwareVers.put("key", "softwareVers");
				info_softwareVers.put("value", smartDevice.getSmartLinkSoftwareVersion());
				wifimoduleInfoArray.put(info_softwareVers);

				//configVers
				JSONObject info_configVers = new JSONObject();
				info_configVers.put("key", "configVers");
				info_configVers.put("value", smartDevice.getSmartLinkDevfileVersion());
				wifimoduleInfoArray.put(info_configVers);

				//protocolVers
				JSONObject info_protocolVers = new JSONObject();
				info_protocolVers.put("key", "protocolVers");
				info_protocolVers.put("value", smartDevice.getEProtocolVer());
				wifimoduleInfoArray.put(info_protocolVers);

				//platform
				JSONObject info_platform = new JSONObject();
				info_platform.put("key", "platform");
				//info_platform.put("value", smartDevice.getSmartlinkPlatform());
				wifimoduleInfoArray.put(info_platform);

				//supportUpgrade
				JSONObject info_supportUpgrade = new JSONObject();
				info_supportUpgrade.put("key", "supportUpgrade");
				info_supportUpgrade.put("value", "1");
				wifimoduleInfoArray.put(info_supportUpgrade);

				//
				jArray.put(deviceJob);

			}
			job.put("devices", jArray);
			return job.toString();

		} catch (JSONException e) {
			e.printStackTrace();

		}
		return null;
	}

}

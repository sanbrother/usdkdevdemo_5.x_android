package com.haier.uhome.ubic.usdk.demo;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;

import com.haier.uhome.usdk.api.interfaces.IuSDKDeviceManagerListener;
import com.haier.uhome.usdk.api.uSDKCloudConnectionState;
import com.haier.uhome.usdk.api.uSDKDevice;
import com.haier.uhome.usdk.api.uSDKDeviceManager;

public class IntroduceuSDKActivity extends Activity {
	
	private WebView tipApiHtmPage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.introusdk_layout);
		
		initUI();
		initUIEvent();
	}

	private void initUIEvent() {
		
	}

	private void initUI() {
		tipApiHtmPage = (WebView) findViewById(R.id.webview);
		String country = Locale.getDefault().getCountry();

		if (country.equalsIgnoreCase("CN")) {
			tipApiHtmPage.loadUrl("file:///android_asset/tipAPI_zh.html");

		} else if (country.equalsIgnoreCase("TW") ||
				country.equalsIgnoreCase("HK")) {
			tipApiHtmPage.loadUrl("file:///android_asset/tipAPI_tw.html");

		} else {
			tipApiHtmPage.loadUrl("file:///android_asset/tipAPI_en.html");

		}
	}

}

package com.haier.uhome.ubic.usdk.demo.uAccount.adapter;

import android.content.Context;

import com.haier.uhome.account.api.interfaces.IAccountListener;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.util.Util;

/**
 * Created by atxd163 on 16-9-16.
 */
public abstract class UAccountRegisterOrLoginLogicAdapter implements IAccountListener<String> {
    @Override
    public void onResponseSuccess(String data) {
        actionLogic(data);
    }

    @Override
    public void onResponseFailed(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx,
                ctx.getString(R.string.tip_webquest_error)
                        + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    @Override
    public void onHttpError(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx,
                ctx.getString(R.string.tip_http_error)
                + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    public UAccountRegisterOrLoginLogicAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public abstract void actionLogic(String param);

    private Context ctx;
}

package com.haier.uhome.ubic.usdk.demo.uAccount.adapter;

import android.content.Context;

import com.haier.uhome.account.api.interfaces.IAccountListener;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.account.model.uacmodel.UacUserBase;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.util.Util;

/**
 * Created by atxd163 on 16-9-17.
 */
public abstract class UAccountUserBaseInfoLogicAdapter implements IAccountListener<UacUserBase> {
    @Override
    public void onResponseSuccess(UacUserBase data) {
        actionLogic(data);
    }

    @Override
    public void onResponseFailed(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx,
                ctx.getString(R.string.tip_webquest_error)
                        + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    @Override
    public void onHttpError(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx, ctx.getString(R.string.tip_http_error)
                + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    public abstract void actionLogic(UacUserBase data);

    public UAccountUserBaseInfoLogicAdapter(Context ctx) {
        this.ctx = ctx;
    }

    private Context ctx;
}

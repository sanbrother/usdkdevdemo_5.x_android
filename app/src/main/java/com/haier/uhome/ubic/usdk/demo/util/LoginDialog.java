package com.haier.uhome.ubic.usdk.demo.util;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.haier.uhome.ubic.usdk.demo.R;

/**
 * Created by admin on 2018/4/3.
 */
public class LoginDialog extends Dialog{
    private Button cancelBt;
    private TextView dialogContent;

    public LoginDialog(Context context) {
        super(context, R.style.notitle_dialog);
        setCancelable(false);
        setContentView(R.layout.login_info_dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cancelBt = (Button) findViewById(R.id.login_dismiss_bt);
        cancelBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginDialog.this.dismiss();
            }
        });

        dialogContent = (TextView) findViewById(R.id.login_desc_tv);

    }

    @Override
    protected void onStart() {
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getWindow().setLayout(
                (int) (dm.widthPixels*0.75), ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void setDialogContent(String dialogContent) {
        this.dialogContent.setText(dialogContent);
    }
}

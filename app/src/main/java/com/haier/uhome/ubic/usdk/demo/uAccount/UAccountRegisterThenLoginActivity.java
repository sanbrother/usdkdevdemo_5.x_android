package com.haier.uhome.ubic.usdk.demo.uAccount;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.haier.uhome.account.api.uAccount;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.ubic.usdk.demo.MainActivity;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.http.UserAccount;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountCommonLogicAdapter;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountRegisterOrLoginLogicAdapter;
import com.haier.uhome.ubic.usdk.demo.util.AppDemo;

/**
 * 演示uAccount lib:
 * 申請短信驗證碼，實現設置密碼注冊登錄
 *
 * Express uAccount lib function:
 * 1.request sms with checkcode
 * 2.set password with checkcode
 * 3.login
 */
public class UAccountRegisterThenLoginActivity extends Activity {
    private TextView activecodeForRegisterTextView;
    private TextView phoneNoForRegisterTextView;
    private Button requestActiveCodeForRegisterButton;
    private TextView passwordEditText;
    private Button execRegisterButton;

    private uAccount accountHelper = uAccount.getSingleInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uaccount_register_then_login_layout);
        init();
        initEvent();

    }

    private void init() {
        phoneNoForRegisterTextView = (TextView)
                findViewById(R.id.account_phoneno_for_register_textview);
        requestActiveCodeForRegisterButton = (Button)
                findViewById(R.id.account_register_request_activecode_button);
        passwordEditText = (TextView)
                findViewById(R.id.account_set_pwd_edittext);
        activecodeForRegisterTextView = (TextView)
                findViewById(R.id.account_activecode_for_register_textview);
        execRegisterButton = (Button)
                findViewById(R.id.account_register_and_login_button);
    }


    private void initEvent() {
        /**
         * 獲得短訊驗證碼
         * Get sms checkcode
         */
        requestActiveCodeForRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNo = phoneNoForRegisterTextView.getText().toString();
                accountHelper.applyActivationCode(getBaseContext(), phoneNo,
                        new UAccountCommonLogicAdapter(UAccountRegisterThenLoginActivity.this) {
                    @Override
                    public void actionLogic(RespCommonModel data) {
                        System.out.println("uAccount request sms checkcode success!");

                    }
                });

            }

        });

        /**
         * 設置密碼并登錄
         * 1.set password with checkcode
         * 2.login, get session(accesstoken)
         */
        execRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String activecode_for_register = activecodeForRegisterTextView.getText().toString();
                String password = passwordEditText.getText().toString();

                accountHelper.registerAndlogin(getBaseContext(), password, activecode_for_register,
                        new UAccountRegisterOrLoginLogicAdapter(UAccountRegisterThenLoginActivity.this) {
                    @Override
                    public void actionLogic(String param) {
                        String accountToken = accountHelper.getAccessToken();
                        fillAccountToken(accountToken);

                    }
                });

            }

        });

    }

    private void fillAccountToken(String accountToken) {
        AppDemo appDemo = (AppDemo) getApplicationContext();
        UserAccount account = appDemo.getUserAccount();
        account.setSession(accountToken);
        loginOKJumpToMainActivity();

    }

    private void loginOKJumpToMainActivity() {
        Intent loginedIntent = new Intent();
        loginedIntent.putExtra("login", true);
        loginedIntent.setClass(this, MainActivity.class);
        startActivity(loginedIntent);

    }
}

package com.haier.uhome.ubic.usdk.demo.http;

/**
 * Created by admin on 2016/5/10.
 */
public interface IhttpActionResultCallback {

    public void loginSuccess();

    public void getDevicesOfAccountSucess();

    public void logoutSuccess();

    public void failed(String msg);

    public void removeDeviceFromAccountSucess();

    public void addDevice2AccountSucess();
}

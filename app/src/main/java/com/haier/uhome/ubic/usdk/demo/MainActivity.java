package com.haier.uhome.ubic.usdk.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.haier.uhome.account.api.uAccount;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.account.model.uacmodel.UacDevice;
import com.haier.uhome.ubic.usdk.demo.adapter.AppManagedDevicesListViewAdapter;
import com.haier.uhome.ubic.usdk.demo.http.UserAccount;
import com.haier.uhome.ubic.usdk.demo.uAccount.UAccountBaseInfoActivity;
import com.haier.uhome.ubic.usdk.demo.uAccount.UAccountEntryCenterActivity;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountCommonLogicAdapter;
import com.haier.uhome.ubic.usdk.demo.util.AppDemo;
import com.haier.uhome.ubic.usdk.demo.util.Util;
import com.haier.uhome.usdk.api.interfaces.IuSDKCallback;
import com.haier.uhome.usdk.api.interfaces.IuSDKDeviceManagerListener;
import com.haier.uhome.usdk.api.uSDKCloudConnectionState;
import com.haier.uhome.usdk.api.uSDKDevice;
import com.haier.uhome.usdk.api.uSDKDeviceManager;
import com.haier.uhome.usdk.api.uSDKDeviceTypeConst;
import com.haier.uhome.usdk.api.uSDKErrorConst;
import com.haier.uhome.usdk.api.uSDKLogLevelConst;
import com.haier.uhome.usdk.api.uSDKManager;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

/**
 * 顯示本地或包括賬號所屬的智能設備列表信息
 */
public class MainActivity extends Activity {
	private uSDKManager uSDKMgr
			= uSDKManager.getSingleInstance();
	private uSDKDeviceManager uSDKDeviceMgr
			= uSDKDeviceManager.getSingleInstance();
	
	private TextView cloudIPTextView = null;
	private ListView sightedDeviceListView = null;
	private AppManagedDevicesListViewAdapter appManagedDevicesListViewAdapter 
				= new AppManagedDevicesListViewAdapter(null);
	
	private List<uSDKDevice> allSightedDevices = null;
	
	private Button toSmartLinkViewButton = null;
	private Button toSoftAPViewButton = null;
	private Button toBTViewButton = null;

	private AppDemo appDemo;

	private uAccount accountHelper = uAccount.getSingleInstance();

	/*
	 * UI知道权限被阻止的Handler
	private Handler permissionLackHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Intent intent = new Intent(
					MainActivity.this, PermissionTipActivity.class);
			startActivity(intent);
		}

	};*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main_layout);
		
		initUI();
		//setHandler2ApplicationForPermissionDenied();
		initUIEvent();
		initDeviceInteractive();

	}

	/**
	 * 開啟發現未入網智能設備的功能
	 * begin to search unjoined smart device
	 */
	private void initAutoFoundUnjoinedSmartDevice() {
		appDemo = (AppDemo) this.getApplication();
		appDemo.helpSmartDeviceAutoJoinWifi(uSDKDeviceMgr);
	}

	private void initUI() {
		cloudIPTextView = (TextView)
				this.findViewById(R.id.cloud_desc_tv);
		toSmartLinkViewButton = (Button) 
				this.findViewById(R.id.toSmartLink_bt);;
		toSoftAPViewButton = (Button)
						this.findViewById(R.id.toSoftAP_bt);
		toBTViewButton = (Button)
						this.findViewById(R.id.toBT_bt);

		sightedDeviceListView = (ListView) 
				this.findViewById(R.id.sighted_deviceslist_lv);
		sightedDeviceListView.setAdapter(appManagedDevicesListViewAdapter);

	}

	/*
	 * UI可以知道权限被阻止
	 *
	private void setHandler2ApplicationForPermissionDenied() {
		AppDemo appDemo = (AppDemo) getApplication();
		appDemo.setMainActivityMsgHandler(this.permissionLackHandler);
	}*/
	
	private void initUIEvent() {
		
		/**
		 * 跳轉下一個頁面連接智能設備
		 * press to jump next view to connect
		 */
		sightedDeviceListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,
									long arg3) {

				uSDKDevice clickeduSDKDevice = (uSDKDevice)
						adapterView.getAdapter().getItem(position);

				if(clickeduSDKDevice.getType() != uSDKDeviceTypeConst.COMMERCIAL_AIRCONDITION) {
					Intent intent = new Intent(MainActivity.this, DeviceStatusActivity.class);
					intent.putExtra("curuSDKDeviceId", clickeduSDKDevice.getDeviceId());
					startActivity(intent);

				} else {
					Intent intent = new Intent(MainActivity.this, ChildDeviceStatusActivity.class);
					intent.putExtra("curuSDKDeviceId", clickeduSDKDevice.getDeviceId());
					startActivity(intent);

				}

			}
		});

		/**
		 * 跳轉至SmartLink配置頁面
		 * to smartlink view
		 */
		toSmartLinkViewButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,
						MakeDevice2NetSmartLink.class);
				boolean isLogined = Util.isAccountLogin(MainActivity.this);
				if(isLogined) {
					startActivity(intent);

				} else {
					Toast.makeText(
							MainActivity.this,
							"绑定设备需要您登录！",
							Toast.LENGTH_SHORT).show();
					jump2AccountInfoCenter();

				}
			}
		});

		/**
		 * 跳轉至SoftAP配置頁面
		 * to softap view
		 * 使用设备发起绑定时需要账号登录
		 * 设备发起绑定兼容普通SoftAP
		 */
		toSoftAPViewButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean isAccountLogin =
						Util.isAccountLogin(MainActivity.this);
				if(isAccountLogin) {
					Intent intent = new Intent(MainActivity.this,
							MakeDevice2NetSoftAP.class);
					startActivity(intent);

				} else {
					Toast.makeText(
							MainActivity.this,
							"绑定设备需要您登录！",
							Toast.LENGTH_SHORT).show();
					jump2AccountInfoCenter();
				}

			}
		});

		toBTViewButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				boolean isAccountLogin =
						Util.isAccountLogin(MainActivity.this);
				if(isAccountLogin) {
					Intent intent = new Intent(MainActivity.this,
							MakeDevice2NetBlueTooth.class);
					startActivity(intent);

				} else {
					Toast.makeText(
							MainActivity.this,
							"绑定设备需要您登录！",
							Toast.LENGTH_SHORT).show();
					jump2AccountInfoCenter();
				}
			}
		});

		cloudIPTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				refreshCloudIP();
			}
		});

	}


	/**
	 * <pre>
	 * 注冊設備管理
	 * 啟動uSDK
	 *
	 * 1.register uSDK smart device managment listner
	 * 2.start uSDK
	 * </pre>
	 */
	private void initDeviceInteractive() {
		registerDevicesManageLogic();
		this.startuSDK();
		refreshCloudIP();

	}

	/**
	 * 啟動uSDK
	 * start uSDK
	 */
	private void startuSDK() {
		uSDKMgr.initLog(uSDKLogLevelConst.USDK_LOG_DEBUG, false, new IuSDKCallback() {
			@Override
			public void onCallback(uSDKErrorConst result) {

			}
		});

		uSDKMgr.startSDK(new IuSDKCallback() {

			@Override
			public void onCallback(uSDKErrorConst uSDKStartResult) {
				//使用此API查询uSDK启动状态
				//Is uSDK started?
				//boolean appQueryuSDKisStatus = uSDKMgr.isSDKStart();
				if (uSDKStartResult == uSDKErrorConst.RET_USDK_OK) {
					//initAutoFoundUnjoinedSmartDevice();
					refreshSightedDeviceList(null);

					String usdkVer = uSDKMgr.getuSDKVersion();
					Util.displayShortMsg(MainActivity.this, "uSDK Start OK, Ver:" + usdkVer);

				} else {
					Util.displayShortMsg(MainActivity.this, "uSDK Start Failed!");

				}

			}

		});
		
	}
	

	private void registerDevicesManageLogic() {
		uSDKDeviceMgr.setDeviceManagerListener(new IuSDKDeviceManagerListener() {

			/**
			 * 設備管理業務-uSDK剔除无法交互的设备
			 * smart devices become unsighted
			 */
			@Override
			public void onDevicesRemove(List<uSDKDevice> devicesChanged) {
				refreshSightedDeviceList(devicesChanged);
				System.out.println("smart device has been invalid....");

			}

			/**
			 * 設備管理業務-uSDK發現一臺可用設備
			 * Device Management-uSDK device report event: 
			 */
			@Override
			public void onDevicesAdd(List<uSDKDevice> devicesChanged) {
				refreshSightedDeviceList(devicesChanged);
				System.out.println("smart device added....");

			}

			/**
			 * <pre>
			 * 遠程服務器推送設備解綁定消息
			 * remote server push a msg when register a smart device on user's account
			 * app need to connect remote server first.
			 * </pre>
			 */
			@Override
			public void onDeviceUnBind(String devicesChanged) {
				System.out.println("Remote Server push msg:"
						+ devicesChanged + " has been removed from accout");

			}

			/**
			 * <pre>
			 * 遠程服務器推送設備綁定消息
			 * remote server push a msg when unregister a smart device from user's account
			 * app need to connect remote server first.
			 * </pre>
			 */
			@Override
			public void onDeviceBind(String devicesChanged) {
				System.out.println("Remote Server push msg:"
						+ devicesChanged + " has been added to accout");

			}

			/**
			 * <pre>
			 * App在此可以得到遠程服務器連接狀態
			 * App get connection status of remote server here
			 * </pre>
			 */
			@Override
			public void onCloudConnectionStateChange(uSDKCloudConnectionState status) {
				//遠程服務器已連接
				//remote server connected
				if (status ==
						uSDKCloudConnectionState.CLOUD_CONNECTION_STATE_CONNECTED) {
					Log.d(AppDemo.TAG, " connect to Remote Server : OK");

				}

			}
		});

	}
	
	private void refreshSightedDeviceList(List<uSDKDevice> devicesChanged) {
		//此时账号未登录仅能发现同一路由下的设备
		allSightedDevices = uSDKDeviceMgr.getDeviceList();
		appManagedDevicesListViewAdapter.setDataSource(allSightedDevices);
		appManagedDevicesListViewAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.option, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case R.id.menu_usdk_api:
	    		Intent intent = new Intent(MainActivity.this, IntroduceuSDKActivity.class);
				startActivity(intent);
	    		break;

			case R.id.menu_account_login:
				jump2AccountInfoCenter();
				break;

	    }
	    return super.onOptionsItemSelected(item);
	}

	private void jump2AccountInfoCenter() {
		AppDemo appDemo = (AppDemo) getApplication();
		UserAccount account = appDemo.getUserAccount();
		Intent intent2JumpAccountEntry = new Intent();
		if(account.isLogin() == true) {
			intent2JumpAccountEntry.setClass(this, UAccountBaseInfoActivity.class);

		} else {
			intent2JumpAccountEntry.setClass(this, UAccountEntryCenterActivity.class);

		}

		startActivity(intent2JumpAccountEntry);

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppDemo appDemo = (AppDemo) getApplication();
		UserAccount account = appDemo.getUserAccount();
		if(account.isLogin() == true) {
			MenuItem accountLoginItem = menu.findItem(R.id.menu_account_login);
			accountLoginItem.setIcon(R.drawable.account_logined);

		} else {
			MenuItem accountLoginItem = menu.findItem(R.id.menu_account_login);
			accountLoginItem.setIcon(R.drawable.account);

		}

		return super.onPrepareOptionsMenu(menu);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			stopHiWifiDeviceScan();
			dealAccountLogout();

		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 賬號登出
	 * Account logout
	 */
	private void dealAccountLogout() {
		AppDemo appDemo = (AppDemo) getApplicationContext();
		UserAccount account = appDemo.getUserAccount();
		boolean isLogin = account.isLogin();

		if(isLogin) {
			//关闭于U+云连接，停止uSDK
			turnOffRemoteServerConnection();
			//清除Token和设备信息
			accountHelper.logout(this,
					new UAccountCommonLogicAdapter(MainActivity.this) {
						@Override
						public void actionLogic(RespCommonModel data) {
							clearAccountTokenAndSetAccountLogout();

						}

					});

		} else {
			stopuSDK();

		}

	}

	private void clearAccountTokenAndSetAccountLogout() {
		AppDemo appDemo = (AppDemo) getApplicationContext();
		UserAccount account = appDemo.getUserAccount();
		account.setSession(null);
		account.setDevicesOfAccount(null);

	}

	/**
	 * 停止极路由设备扫描
	 */
	private void stopHiWifiDeviceScan(){
		uSDKDeviceMgr.stopScanConfigurableDevice(new IuSDKCallback() {
			@Override
			public void onCallback(uSDKErrorConst result) {
				Log.d("uSDK Demo:", "stop scan" + result);
			}
		});

	}

	/**
	 * 停止uSDK
	 */
	private void stopuSDK() {
		uSDKMgr.stopSDK(new IuSDKCallback() {

			@Override
			public void onCallback(uSDKErrorConst result) {
				Log.d(AppDemo.TAG, "stop uSDK result:" + result);

			}
		});
	}

	/**
	 * 断开远程服务器连接
	 * close connection of remote server
	 */
	private void turnOffRemoteServerConnection() {
		uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
		deviceManager.disconnectToGateway(new IuSDKCallback() {
			@Override
			public void onCallback(uSDKErrorConst result) {
				Log.d("uSDK Demo:", "disconnect gateway:" + result);
				if(result != uSDKErrorConst.RET_USDK_OK) {
					System.err.println(
							"uSDKDemo App exec disconnectToGateway failed: " + result);
				}

				stopuSDK();

			}
		});
	};

	private void refreshCloudIP() {
		CloudEnvironmentIPTask cloudEnvironmentIPTask = new CloudEnvironmentIPTask();
		cloudEnvironmentIPTask.execute();
	}


	private class CloudEnvironmentIPTask extends AsyncTask<String, Void, String> {
		/** The system calls this to perform work in a worker thread and
		 * delivers it the parameters given to AsyncTask.execute() */
		protected String doInBackground(String... urls) {
			String environmentDesc = null;
			try {
				InetAddress environmentInetAddress =
						InetAddress.getByName("gw.haier.net");
				environmentDesc = environmentInetAddress.getHostAddress();
				System.out.println("Current Cloud Environment:" + environmentDesc);

			} catch (UnknownHostException e) {
				e.printStackTrace();
				return "Cloud IP Get Failed!";

			}

			return environmentDesc;
		}

		/** The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground() */
		protected void onPostExecute(String result) {
			cloudIPTextView.setText("gw.haier.net:" + result);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		AppDemo demo = (AppDemo) getApplication();
		UserAccount account = demo.getUserAccount();

		if(account.isLogin()) {
			uSDKCloudConnectionState cloudConnectionState =
					uSDKDeviceMgr.getCloudConnectionState();

			if(cloudConnectionState !=
					uSDKCloudConnectionState.CLOUD_CONNECTION_STATE_CONNECTED) {
				connect2RemoteServerWhenLogin();

			}

		} else {
			setTitle(R.string.app_name);

		}

		invalidateOptionsMenu();

	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	private void connect2RemoteServerWhenLogin() {
		AppDemo appDemo = (AppDemo) getApplicationContext();
		UserAccount userAccount = appDemo.getUserAccount();

		UacDevice[] devicesOfAccount = userAccount.getDevicesOfAccount();
		String token = userAccount.getSession();

		uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
		List remotedCtrledDeviceCollection =
				Util.fillDeviceInfoForRemoteControl(devicesOfAccount);
		deviceManager.connectToGateway(token, "gw.haier.net", 56811,
				remotedCtrledDeviceCollection,
				new IuSDKCallback() {

					@Override
					public void onCallback(uSDKErrorConst result) {
						if(result != uSDKErrorConst.RET_USDK_OK) {
							System.err.println("uSDK3XDemo exec connectToGateway method failed! " + result);
						}
					}
				});

	}

}

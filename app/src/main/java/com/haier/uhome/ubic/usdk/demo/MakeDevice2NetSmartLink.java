package com.haier.uhome.ubic.usdk.demo;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.haier.uhome.trace.api.Trace;
import com.haier.uhome.ubic.usdk.demo.util.AppDemo;
import com.haier.uhome.ubic.usdk.demo.util.SmartConfigDialog;
import com.haier.uhome.ubic.usdk.demo.util.WifiUtils;
import com.haier.uhome.usdk.api.interfaces.IuSDKCallback;
import com.haier.uhome.usdk.api.uSDKDevice;
import com.haier.uhome.usdk.api.uSDKDeviceManager;
import com.haier.uhome.usdk.api.uSDKErrorConst;
import com.haier.uhome.usdk.base.api.uSDKError;
import com.haier.uhome.usdk.bind.BindProgress;
import com.haier.uhome.usdk.bind.Binding;
import com.haier.uhome.usdk.bind.IBindResultCallback;
import com.haier.uhome.usdk.bind.SmartLinkBindInfo;


/**
 * 演示配置智能設備加入網路
 * Smartlink smart device to connect to wifi
 */
public class MakeDevice2NetSmartLink extends Activity {

	private Trace bussinessTrace = AppDemo.trace;

	private EditText wifiNameEditText;
	private EditText wifiPasswordEeditText;
	private Button confirmButton;
	private TextView configResultInfoTextView;
	private SmartConfigDialog configProgressDialog = null;

	private String wifiName;
	private String wifiPass;
	
	private String waiting2joinwifidevicemac;
	
	private uSDKDeviceManager uSDKDeviceMgr = uSDKDeviceManager.getSingleInstance();

	private uSDKDevice ready2AccountDevice;

	private Binding bindUtil = Binding.getSingleInstance();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.makedevice2net_smartlink_layout);
		
		initUI();
		initUIEvent();
		isWaitingJoinWifiDeviceDeal();
	}

	/**
	 * 
	 */
	private void isWaitingJoinWifiDeviceDeal() {
		Intent intentFromNotification = getIntent();
		String tailOfMac = intentFromNotification.getStringExtra("mac");
		if(intentFromNotification != null && tailOfMac != null) {
			waiting2joinwifidevicemac = tailOfMac;

		}
	}

	private void initUI() {
		wifiNameEditText = (EditText) 
				findViewById(R.id.wifiname_et);
		wifiPasswordEeditText = (EditText) 
				findViewById(R.id.wifipassword_et);
		confirmButton = (Button) 
				findViewById(R.id.device2net_bt);
		configResultInfoTextView = (TextView) 
				findViewById(R.id.tip_device2net_result_tv);

		String wifiName = WifiUtils.getWifiTitle(this);
		if (TextUtils.isEmpty(wifiName)) {
			wifiNameEditText.setHint(this.getString(R.string.tip_wifioff));

		} else {
			wifiNameEditText.setText(wifiName);

		}
		
		this.configProgressDialog = new SmartConfigDialog(this);

	}

	private void initUIEvent() {
		confirmButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				wifiName = wifiNameEditText.getEditableText().toString();
				if (checkApName(wifiName)) {
					exec_smartlink_config();
					
				}
				
			}
			
		});
		
		/**
		 * 取消智能設備配置入網動作
		 * cancel smartlink progress
		 */
		configProgressDialog.setOnCancelListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				uSDKDeviceMgr.stopSmartLinkConfig(new IuSDKCallback() {
					
					@Override
					public void onCallback(uSDKErrorConst result) {
						System.out.println("config:" + result);
						if(configProgressDialog != null) {
							configProgressDialog.dismiss();
						}
					
					}
				});
				
			}
		});
	}

	private void showDevice(uSDKDevice device) {
		if (device != null) {
			StringBuffer deviceInfo = new StringBuffer();
			deviceInfo.append(getString(R.string.code_config_ok_device));
			deviceInfo.append("\r\n");
			deviceInfo.append(device.getType().getValue());
			deviceInfo.append(" | ");
			deviceInfo.append(device.getDeviceId());
			deviceInfo.append(" | ");
			deviceInfo.append(device.getIp());
			configResultInfoTextView.setText(deviceInfo.toString());
		} 
	}
	
	private void showConfigFailedInfo(uSDKError reason) {
		StringBuffer failedInfo = new StringBuffer();
		failedInfo.append(getString(R.string.code_config_failture));
		failedInfo.append(reason.getCode());
		failedInfo.append("，");
		failedInfo.append(reason.getDescription());
		configResultInfoTextView.setText(failedInfo);
	}

	private void exec_smartlink_config() {
		
		clearConfigResult();

		wifiName = wifiNameEditText.getEditableText().toString();
		wifiPass = wifiPasswordEeditText.getEditableText().toString();
		
		configProgressDialog.show();

		/**
		 * exec smartlink with wifi(name、pass、mac), then set timeout 60s
		 * set timeout 90s when device type is air condition or air cleaner
		 * false:support safety and common uPlug, true:safety only
		 *
		 * 執行智能設備配置入網：空調、空氣凈化器類設備使用90s超時
		 * false：支持安全產品和非安全產品（upLug)，true：僅支持安全uPlug
		 */
			SmartLinkBindInfo smartLinkBindInfo = new SmartLinkBindInfo.Builder()
					.routerInfo(wifiName, wifiPass)
					.timeout(60)
					.security(false)
					//.deviceId() 指定deviceId时使用
					//.uplusIds() 指定uplusId时使用
					.build();

			bindUtil.bindDeviceBySmartLink(
					smartLinkBindInfo,
					new IBindResultCallback<uSDKDevice>() {

				@Override
				public void notifyProgress(
						BindProgress bindProgress, String devId, String uplusId) {
					Log.d(AppDemo.TAG,
							"Process:"
									+ devId
									+ "-"
									+ bindProgress.getStateDescription()
									+ "-" + uplusId);
					Toast.makeText(
							MakeDevice2NetSmartLink.this,
							"正在为您：" + bindProgress.getStateDescription(),
							Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onSuccess(uSDKDevice bindedDevice) {
					Log.d(AppDemo.TAG, bindedDevice.getDeviceId() + " has joined the network!");
					showDevice(bindedDevice);
					ready2AccountDevice = bindedDevice;
					configProgressDialog.dismiss();
				}

				@Override
				public void onFailure(uSDKError errInfo) {
					Log.d(AppDemo.TAG,
							errInfo.toString()
									+ "-" + errInfo.getCode()
									+ "-" + errInfo.getFailureReason()
									+ "-" + errInfo.getDescription());
					configProgressDialog.dismiss();
					showConfigFailedInfo(errInfo);
					Toast.makeText(
							MakeDevice2NetSmartLink.this,
							"绑定失败：" + errInfo.getDescription(),
							Toast.LENGTH_SHORT).show();
				}

			});
	}
	
	private boolean checkApName(String apName) {
		if (apName != null && apName.length() != 0) {
			return true;

		} else {
			Toast.makeText(this, getString(R.string.code_check_wifiname), 
					Toast.LENGTH_LONG).show();
			return false;

		}
	}
	
	private void clearConfigResult() {
		configResultInfoTextView.setText("");
	}

}
package com.haier.uhome.ubic.usdk.demo.uAccount;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.haier.uhome.account.api.uAccount;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountCommonLogicAdapter;

/**
 * 演示uAccount lib：
 * 郵箱賬號綁定手機號碼
 *
 * Express uAccount lib funcation:
 * 1.request sms checkcode
 * 2.fill mobile phone in U+Cloud Account
 *
 */
public class UEmailAccountBindMobileActivity extends Activity {
    private EditText email_for_emailaccount_bindphoneno_TextView;
    private EditText phoneNo_for_emailaccount_bindphoneno_TextView;
    private Button requestCheckCodeForEmailAccountButton;
    private EditText activeCode_for_emailaccount_bindPhoneNo_TextView;
    private Button execBindMobileNoButton;

    private uAccount accountHelper = uAccount.getSingleInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uemailaccount_bind_mobile_activity);
        init();
        initEvent();
    }

    private void init() {
        email_for_emailaccount_bindphoneno_TextView = (EditText)
                findViewById(R.id.emailaccount_email_textview);
        phoneNo_for_emailaccount_bindphoneno_TextView = (EditText)
                findViewById(R.id.account_phoneno_textview);
        requestCheckCodeForEmailAccountButton = (Button)
                findViewById(R.id.account_update_email_or_phone_apply_activecode_button);
        execBindMobileNoButton = (Button)
                findViewById(R.id.account_update_email_or_phone_button);

        activeCode_for_emailaccount_bindPhoneNo_TextView = (EditText)
                findViewById(R.id.account_email_or_phone_bind_activecode_textview);

    }

    private void initEvent() {
        requestCheckCodeForEmailAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestActiveCodeForEmailAccount();

            }
        });

        execBindMobileNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execBindMobileNo();

            }
        });

    }


    private void requestActiveCodeForEmailAccount() {
        String email = email_for_emailaccount_bindphoneno_TextView.getText().toString();
        String phoneNo = phoneNo_for_emailaccount_bindphoneno_TextView.getText().toString();

        /**
         * 申請短訊驗證碼
         * request a sms with check code
         */
        accountHelper.applyBindMobileCode(this, email, phoneNo,
                new UAccountCommonLogicAdapter(UEmailAccountBindMobileActivity.this) {
            @Override
            public void actionLogic(RespCommonModel data) {
                System.out.println("email account bind phone no success!");

            }
        });
    }

    private void execBindMobileNo() {
        String activeCode = activeCode_for_emailaccount_bindPhoneNo_TextView.getText().toString();
        /**
         * 綁定移動電話號碼
         * fill in phoneno with checkcode
         */
        accountHelper.bindMobile(this, activeCode,
                new UAccountCommonLogicAdapter(UEmailAccountBindMobileActivity.this) {
            @Override
            public void actionLogic(RespCommonModel data) {
                System.out.println("email account bind phone no success!");

            }
        });
    }
}

package com.haier.uhome.ubic.usdk.demo.uAccount;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.haier.uhome.account.api.interfaces.IAccountListener;
import com.haier.uhome.account.api.uAccount;
import com.haier.uhome.account.model.uacmodel.UacDevice;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.account.model.uacmodel.UacUserBase;
import com.haier.uhome.ubic.usdk.demo.MainActivity;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.http.UserAccount;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountCommonLogicAdapter;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountGetUCloudDeviceListAdapter;
import com.haier.uhome.ubic.usdk.demo.uAccount.adapter.UAccountUserBaseInfoLogicAdapter;
import com.haier.uhome.ubic.usdk.demo.util.AppDemo;
import com.haier.uhome.ubic.usdk.demo.util.DeviceNameModifyDialog;
import com.haier.uhome.ubic.usdk.demo.util.Util;
import com.haier.uhome.usdk.api.interfaces.IuSDKCallback;
import com.haier.uhome.usdk.api.uSDKDeviceManager;
import com.haier.uhome.usdk.api.uSDKErrorConst;

import java.util.List;

/**
 * 演示uAccount lib功能：
 * 注銷、加載賬號設備列表、修改某智能設備名稱、重置密碼、
 * 拉取用戶基本信息
 *
 * Express uAccount lib function:
 * logout, Get Account's smart device info, Modify smart device's nickname
 * reset password、Get Account's Base Info
 */
public class UAccountBaseInfoActivity extends Activity {
    private TabHost tabhost;

    private TextView accountUserId;
    private TextView accountName;
    private TextView accountPhone;
    private TextView accountEmail;
    private Button jumpEmailAccountBindPhoneActivityButton;

    //显示设备列表和修改设备名称
    private ListView smartdevicesOfAccountLV;
    private ArrayAdapter devicesOfAccountAdapter;

    private Button accountLogoutBT;
    private Button accountResetPasswordBT;

    private uAccount accountHelper = uAccount.getSingleInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uaccount_baseinfo_activity);
        init();
        initEvent();
    }

    private void init() {
        tabhost = (TabHost) findViewById(R.id.tabHost);
        tabhost.setup();

        accountUserId = (TextView)
                findViewById(R.id.account_baseinfo_userid);
        accountName = (TextView)
                findViewById(R.id.account_baseinfo_username);
        accountPhone = (TextView)
                findViewById(R.id.account_baseinfo_phone);
        accountEmail = (TextView)
                findViewById(R.id.account_baseinfo_mail);
        jumpEmailAccountBindPhoneActivityButton = (Button)
                findViewById(R.id.jump_email_account_bind_phoneno_button);


        smartdevicesOfAccountLV = (ListView)
                findViewById(R.id.account_deviceslist_lv);

        devicesOfAccountAdapter =
                new ArrayAdapter(UAccountBaseInfoActivity.this, android.R.layout.simple_list_item_1);
        smartdevicesOfAccountLV.setAdapter(devicesOfAccountAdapter);

        accountLogoutBT = (Button)
                findViewById(R.id.account_logout_button);
        accountResetPasswordBT = (Button)
                findViewById(R.id.account_reset_password_button);
    }

    private void initEvent() {
        tabhost.addTab(
                tabhost.newTabSpec("tab_test1").
                        setIndicator(getString(R.string.title_list_user_info)).
                        setContent(R.id.linearLayout2));
        tabhost.addTab(
                tabhost.newTabSpec("tab_test2").
                        setIndicator(getString(R.string.title_list_device_binded)).
                        setContent(R.id.linearLayout3));

        smartdevicesOfAccountLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String deviceShortName = (String) parent.getAdapter().getItem(position);
                updateDeviceNickName(deviceShortName);

            }
        });

        accountLogoutBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealAccountLogout();

            }
        });

        accountResetPasswordBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealAccountPasswordReset();

            }
        });


        jumpEmailAccountBindPhoneActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpEmailAccountBindPhoneActivity();

            }
        });
    }

    /**
     * 從U+云獲得賬號基本信息
     * Get Account's Base info
     */
    private void getUserBaseInfo() {
        accountHelper.getUserBaseInfo(this,
                new UAccountUserBaseInfoLogicAdapter(UAccountBaseInfoActivity.this) {
            @Override
            public void actionLogic(UacUserBase data) {
                UacUserBase userBaseInfo = (UacUserBase) data;
                accountUserId.setText(userBaseInfo.getId());
                accountName.setText(userBaseInfo.getLoginName());
                accountPhone.setText(userBaseInfo.getMobile());
                accountEmail.setText(userBaseInfo.getEmail());

            }
        });

    }

    /**
     * 獲得U+云賬號所屬所有智能設備信息
     * Get account's all of smart devcies info
     */
    private void getDevicesOfAccount() {
        accountHelper.getDeviceList(this, null, null,
                new UAccountGetUCloudDeviceListAdapter(UAccountBaseInfoActivity.this) {
            @Override
            public void actionLogic(UacDevice[] data) {
                devicesOfAccountAdapter.clear();
                List devicesOfAccount = Util.fillDeviceInfoForUserInfoView(data);
                devicesOfAccountAdapter.addAll(devicesOfAccount);
                devicesOfAccountAdapter.notifyDataSetChanged();

            }
        });
    }

    /**
     * 修改智能設備昵稱
     * modify smart device's nickname
     */
    private void updateDeviceNickName(final String deviceId) {
        final DeviceNameModifyDialog deviceNameModifyDialog = new DeviceNameModifyDialog(this);
        final String deviceRealId = getDeviceRealId(deviceId);
        deviceNameModifyDialog.setTitle(deviceId);
        deviceNameModifyDialog.show();

        deviceNameModifyDialog.setModifyButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String deviceNewNickName = deviceNameModifyDialog.getDeviceNickName();

                accountHelper.updateNickname(
                        UAccountBaseInfoActivity.this, deviceRealId, deviceNewNickName,
                        new UAccountCommonLogicAdapter(UAccountBaseInfoActivity.this) {
                            @Override
                            public void actionLogic(RespCommonModel data) {
                                System.out.println("uAccount update smartdevice nickname success!");
                                deviceNameModifyDialog.dismiss();
                                getDevicesOfAccount();

                            }
                        });

            }
        });

    }

    private String getDeviceRealId(String deviceId) {
        String[] deviceNameArr = deviceId.split("-");
        if(deviceNameArr.length == 2) {
           return deviceNameArr[0];

        } else {
            return null;

        }

    }

    /**
     * 賬號登出
     * Account logout
     */
    private void dealAccountLogout() {
        accountHelper.logout(this,
                new UAccountCommonLogicAdapter(UAccountBaseInfoActivity.this) {

            @Override
            public void actionLogic(RespCommonModel data) {
                clearAccountTokenAndSetAccountLogout();

            }
        });

    }

    private void clearAccountTokenAndSetAccountLogout() {
        AppDemo appDemo = (AppDemo) getApplicationContext();
        UserAccount account = appDemo.getUserAccount();
        account.setSession(null);
        account.setDevicesOfAccount(null);

        turnOffRemoteServerConnection();
        logoutOkJumpToMainActivity();

    }

    private void logoutOkJumpToMainActivity() {
        Intent loginedIntent = new Intent();
        loginedIntent.setClass(this, MainActivity.class);
        startActivity(loginedIntent);
    }


    /**
     * 断开远程服务器连接
     * close connection of remote server
     */
    private void turnOffRemoteServerConnection() {
        uSDKDeviceManager deviceManager = uSDKDeviceManager.getSingleInstance();
        deviceManager.disconnectToGateway(new IuSDKCallback() {
            @Override
            public void onCallback(uSDKErrorConst result) {
                if(result != uSDKErrorConst.RET_USDK_OK) {
                    System.err.println(
                            "uSDKDemo App exec disconnectToGateway failed: " + result);
                }
            }
        });
    };

    /**
     * 修改密碼
     * modify password
     */
    private void dealAccountPasswordReset() {
        String passwordResetURL = accountHelper.modifyPassword(this);
        Intent passwordResetURLIntent = new Intent();
        passwordResetURLIntent.setAction(Intent.ACTION_VIEW);
        Uri forgetPasswordURI = Uri.parse(passwordResetURL);
        passwordResetURLIntent.setData(forgetPasswordURI);
        startActivity(passwordResetURLIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserBaseInfo();
        getDevicesOfAccount();

    }

    /**
     * 郵箱賬號跳轉頁面綁定移動電話號碼
     * Email account bind mobile phone
     */
    private void jumpEmailAccountBindPhoneActivity() {
        String phoneAccount = accountPhone.getText().toString();
        if(TextUtils.isEmpty(phoneAccount) == true) {
            Intent emailAccountBindPhoneIntent = new Intent();
            emailAccountBindPhoneIntent.setClass(this, UEmailAccountBindMobileActivity.class);
            startActivity(emailAccountBindPhoneIntent);

        } else {
            Util.displayShortMsg(this, "当前账号无需要再次绑定手机号");

        }

    }

}

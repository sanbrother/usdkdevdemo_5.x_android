package com.haier.uhome.ubic.usdk.demo.uAccount.adapter;

import android.content.Context;

import com.haier.uhome.account.model.uacmodel.UacDevice;
import com.haier.uhome.account.api.interfaces.IAccountListener;
import com.haier.uhome.account.model.RespCommonModel;
import com.haier.uhome.ubic.usdk.demo.R;
import com.haier.uhome.ubic.usdk.demo.util.Util;

/**
 * Created by atxd163 on 16-9-17.
 */
public abstract class UAccountGetUCloudDeviceListAdapter implements IAccountListener<UacDevice[]> {
    @Override
    public void onResponseSuccess(UacDevice[] data) {
        actionLogic(data);
    }

    @Override
    public void onResponseFailed(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx,
                ctx.getString(R.string.tip_webquest_error)
                        + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    @Override
    public void onHttpError(RespCommonModel respCommonModel) {
        Util.displayShortMsg(ctx, ctx.getString(R.string.tip_http_error)
                + respCommonModel.getRetCode() + respCommonModel.getRetInfo());
    }

    public abstract void actionLogic(UacDevice[] data);

    public UAccountGetUCloudDeviceListAdapter(Context ctx) {
        this.ctx = ctx;
    }

    private Context ctx;
}
